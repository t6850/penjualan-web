<?php

class Curl
{
	public static function post($api, $request = null)
	{
		$curl = curl_init();

		curl_setopt_array($curl, array(
		CURLOPT_URL => $api,
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => '',
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 0,
		CURLOPT_FOLLOWLOCATION => true,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => 'POST',
		CURLOPT_POSTFIELDS => json_encode($request),
		CURLOPT_HTTPHEADER => array(
			// 'Authorization: Bearer '.$userToken,
			'Content-Type: application/json'
		),
		));

		$response = curl_exec($curl);
		$httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
		curl_close($curl);
		$response = array(
			'httpCode' => $httpCode,
			'response' => $response
		);
		return json_encode($response); 
	}
}

