<?php

class FlashData
{
  public static function successSaved()
  {
    return "
      <script>
        $(function() {
            var Toast = Swal.mixin({
              toast: true,
              position: 'top-end',
              showConfirmButton: false,
              timer: 5000
            });

            $('.swalDefaultSuccess').click(function() {
                Toast.fire({
                  icon: 'success',
                  title: 'Data saved successfully.'
                })
            });
            $('.swalDefaultSuccess').trigger('click');
        });
      </script>";
  }

  public static function failedSaved()
  {
    return "
      <script>
        $(function() {
            var Toast = Swal.mixin({
              toast: true,
              position: 'top-end',
              showConfirmButton: false,
              timer: 5000
            });

            $('.swalDefaultError').click(function() {
                Toast.fire({
                  icon: 'error',
                  title: 'Data failed to saved.'
                })
            });
            $('.swalDefaultError').trigger('click');
        });
      </script>";
  }

  public static function dataNotFound()
  {
    return "
      <script>
        $(function() {
            var Toast = Swal.mixin({
              toast: true,
              position: 'top-end',
              showConfirmButton: false,
              timer: 5000
            });

            $('.swalDefaultWarning').click(function() {
                Toast.fire({
                  icon: 'warning',
                  title: 'Data not found.'
                })
            });
            $('.swalDefaultWarning').trigger('click');
        });
      </script>";
  }

  public static function deletedSuccess()
  {
    return "
      <script>
        $(function() {
            var Toast = Swal.mixin({
              toast: true,
              position: 'top-end',
              showConfirmButton: false,
              timer: 5000
            });

            $('.swalDefaultSuccess').click(function() {
                Toast.fire({
                  icon: 'success',
                  title: 'Deleted data successfully.'
                })
            });
            $('.swalDefaultSuccess').trigger('click');
        });
      </script>";
  }

  public static function deletedFailed()
  {
    return "
      <script>
        $(function() {
            var Toast = Swal.mixin({
              toast: true,
              position: 'top-end',
              showConfirmButton: false,
              timer: 5000
            });

            $('.swalDefaultError').click(function() {
                Toast.fire({
                  icon: 'error',
                  title: 'Deleted data failed.'
                })
            });
            $('.swalDefaultError').trigger('click');
        });
      </script>";
  }

  public static function loginSuccess()
  {
    return "
      <script>
        $(function() {
            var Toast = Swal.mixin({
              toast: true,
              position: 'top-end',
              showConfirmButton: false,
              timer: 5000
            });

            $('.swalDefaultSuccess').click(function() {
                Toast.fire({
                  icon: 'success',
                  title: 'Login successfully.'
                })
            });
            $('.swalDefaultSuccess').trigger('click');
        });
      </script>";
  }

  public static function loginFailed()
  {
    return "
      <script>
        $(function() {
            var Toast = Swal.mixin({
              toast: true,
              position: 'top-end',
              showConfirmButton: false,
              timer: 5000
            });

            $('.swalDefaultError').click(function() {
                Toast.fire({
                  icon: 'error',
                  title: 'Login Failed.'
                })
            });
            $('.swalDefaultError').trigger('click');
        });
      </script>";
  }
}
