<?php
$this->load->view('component/header', $navMenu);
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Channel</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="<?php echo base_url('produk/showproduk') ?>">Produk</a></li>
              <li class="breadcrumb-item active">Add</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- jquery validation -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Store Produk</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form class="needs-validation" action="<?php echo base_url('produkcontroller/storeproduk') ?>" method="POST" novalidate>
                <div class="card-body">
                  <div class="form-group">
                    <label for="channelName">Nama</label>
                    <input type="text" name="nama" class="form-control" id="nama" placeholder="Enter name" required>
                    <div class="invalid-feedback">Pleace enter a valid name.</div>
                    <div class="valid-feedback">Looks good!</div>
                  </div>
                  <div class="form-group">
                    <label for="ownerName">Merk</label>
                    <input type="text" name="merk" class="form-control" id="merk" placeholder="Enter merk" required>
                    <div class="invalid-feedback">Pleace enter a valid merk.</div>
                    <div class="valid-feedback">Looks good!</div>
                  </div>
                  <div class="form-group">
                    <label for="phoneNumber">Harga</label>
                    <input type="number" name="harga" class="form-control" id="harga" placeholder="Enter harga" minlength="1" required>
                    <div class="invalid-feedback">Your harga must be at least 1 characters long.</div>
                    <div class="valid-feedback">Looks good!</div>
                  </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
            <!-- /.card -->
          </div>
          <!--/.col (left) -->
          <!-- right column -->
          <div class="col-md-6">

          </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php
$this->load->view('component/footer');
?>