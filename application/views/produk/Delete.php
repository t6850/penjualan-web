<!-- modal -->
<div class="modal fade" id="modalDelete<?php echo $kode ?>">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header bg-danger">
        <h4 class="modal-title">Confirmation</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form class="needs-validation" action="<?php echo base_url('produkcontroller/deleteproduk') ?>" method="POST" novalidate>
        <div class="modal-body">
          <p>You want to delete this data ?</p>
          <input class="form-control" type="text" name="kode" id="kode" value="<?php echo $kode ?>" hidden>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
          <button type="submit" class="btn btn-primary">Yes</button>
        </div>
      </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->