<?php
$this->load->view('component/header', $navMenu);
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Produk</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item active">Produk</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
    <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Data Produk</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <div class="col-1">
                  <a href="<?php echo base_url('produkcontroller/addproduk') ?>" type="submit" class="btn btn-block bg-gradient-primary">Add</a>
                </div>
                <table id="table" class="table table-bordered table-hover">
                  <thead class="text-center">
                    <tr>
                      <th width="10px">No.</th>
                      <th width="100px">Kode</th>
                      <th width="100px">Nama</th>
                      <th width="100px">Merk</th>
                      <th width="100px">Harga</th>
                      <th width="30px">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                  <?php 
                  $hc = $httpCode;
                  if ($hc == 200) {
                    $no = 1;
                    $getProdukList = $getProdukList->data;
                    foreach ($getProdukList as $getProdukList) {
                      $produkKode = array('kode' => $getProdukList->kode);
                      ?>
                      <tr>
                        <td><?php echo $no++; ?></td>
                        <td><?php echo $getProdukList->kode; ?></td>
                        <td><?php echo $getProdukList->nama; ?></td>
                        <td><?php echo $getProdukList->merk; ?></td>
                        <td><?php echo $getProdukList->harga; ?></td>
                        <td>
                        <a type="submit" href="<?php echo base_url('produkcontroller/optionalproduk/'.$getProdukList->kode) ?>" 
                          class="btn btn-block bg-gradient-warning btn btn-sm">Update</a>
                          <button type="button" class="btn btn-block bg-gradient-danger btn btn-sm" data-toggle="modal" data-target="#modalDelete<?php echo $getProdukList->kode ?>">Delete</button>
                        </td>
                      </tr>
                      <?php $this->load->view('produk/delete', $produkKode);
                    }
                  }
                  ?>
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<button type="submit" class="btn btn-primary swalDefaultSuccess" style="visibility: hidden">success</button>
<button type="submit" class="btn btn-primary swalDefaultError" style="visibility: hidden">failed</button>
<button type="submit" class="btn btn-primary swalDefaultWarning" style="visibility: hidden">warning</button>

<?php
$this->load->view('component/footer');
if ($this->session->flashdata('success')) {
  echo $this->session->flashdata('success');
  unset($_SESSION['success']);
}

if ($this->session->flashdata('failed')) {
  echo $this->session->flashdata('failed');
  unset($_SESSION['failed']);
}

if ($this->session->flashdata('warning')) {
  echo $this->session->flashdata('warning');
  unset($_SESSION['warning']);
}
?>