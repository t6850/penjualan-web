  <!-- Main Footer -->
  <footer class="main-footer">
    <strong>Copyright &copy; 2014-2021 <a href="https://adminlte.io">AdminLTE.io</a>.</strong>
    All rights reserved.
    <div class="float-right d-none d-sm-inline-block">
      <b>Version</b> 3.2.0-rc
    </div>
  </footer>
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->
<!-- jQuery -->
<script src="<?php echo base_url(BashUrlVendor::ALMASAEED2010_URL) ?>/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="<?php echo base_url(BashUrlVendor::ALMASAEED2010_URL) ?>/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- overlayScrollbars -->
<script src="<?php echo base_url(BashUrlVendor::ALMASAEED2010_URL) ?>/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url(BashUrlVendor::ALMASAEED2010_URL) ?>/dist/js/adminlte.js"></script>

<!-- PAGE PLUGINS -->
<!-- jQuery Mapael -->
<script src="<?php echo base_url(BashUrlVendor::ALMASAEED2010_URL) ?>/plugins/jquery-mousewheel/jquery.mousewheel.js"></script>
<script src="<?php echo base_url(BashUrlVendor::ALMASAEED2010_URL) ?>/plugins/raphael/raphael.min.js"></script>
<script src="<?php echo base_url(BashUrlVendor::ALMASAEED2010_URL) ?>/plugins/jquery-mapael/jquery.mapael.min.js"></script>
<script src="<?php echo base_url(BashUrlVendor::ALMASAEED2010_URL) ?>/plugins/jquery-mapael/maps/usa_states.min.js"></script>
<!-- ChartJS -->
<script src="<?php echo base_url(BashUrlVendor::ALMASAEED2010_URL) ?>/plugins/chart.js/Chart.min.js"></script>
<!-- bs-custom-file-input -->
<script src="<?php echo base_url(BashUrlVendor::ALMASAEED2010_URL) ?>/plugins/bs-custom-file-input/bs-custom-file-input.min.js"></script>
<!-- jquery-validation -->
<script src="<?php echo base_url(BashUrlVendor::ALMASAEED2010_URL) ?>/plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="<?php echo base_url(BashUrlVendor::ALMASAEED2010_URL) ?>/plugins/jquery-validation/additional-methods.min.js"></script>

<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url(BashUrlVendor::ALMASAEED2010_URL) ?>/dist/js/demo.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="<?php echo base_url(BashUrlVendor::ALMASAEED2010_URL) ?>/dist/js/pages/dashboard2.js"></script>

<!-- DataTables  & Plugins -->
<script src="<?php echo base_url(BashUrlVendor::ALMASAEED2010_URL) ?>/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(BashUrlVendor::ALMASAEED2010_URL) ?>/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="<?php echo base_url(BashUrlVendor::ALMASAEED2010_URL) ?>/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url(BashUrlVendor::ALMASAEED2010_URL) ?>/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="<?php echo base_url(BashUrlVendor::ALMASAEED2010_URL) ?>/plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url(BashUrlVendor::ALMASAEED2010_URL) ?>/plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="<?php echo base_url(BashUrlVendor::ALMASAEED2010_URL) ?>/plugins/jszip/jszip.min.js"></script>
<script src="<?php echo base_url(BashUrlVendor::ALMASAEED2010_URL) ?>/plugins/pdfmake/pdfmake.min.js"></script>
<script src="<?php echo base_url(BashUrlVendor::ALMASAEED2010_URL) ?>/plugins/pdfmake/vfs_fonts.js"></script>
<script src="<?php echo base_url(BashUrlVendor::ALMASAEED2010_URL) ?>/plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="<?php echo base_url(BashUrlVendor::ALMASAEED2010_URL) ?>/plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="<?php echo base_url(BashUrlVendor::ALMASAEED2010_URL) ?>/plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
<!-- SweetAlert2 -->
<script src="<?php echo base_url(BashUrlVendor::ALMASAEED2010_URL) ?>/plugins/sweetalert2/sweetalert2.min.js"></script>
<!-- Toastr -->
<script src="<?php echo base_url(BashUrlVendor::ALMASAEED2010_URL) ?>/plugins/toastr/toastr.min.js"></script>
<!-- Page specific script -->
<script>
  $(function () {
    $("#example1").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false,
      "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    $('#table').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });

  $(function () {
    bsCustomFileInput.init();
  });

  // Example starter JavaScript for disabling form submissions if there are invalid fields
  (function() {
    'use strict';
    window.addEventListener('load', function() {
      // Fetch all the forms we want to apply custom Bootstrap validation styles to
      var forms = document.getElementsByClassName('needs-validation');
      // Loop over them and prevent submission
      var validation = Array.prototype.filter.call(forms, function(form) {
        form.addEventListener('submit', function(event) {
          if (form.checkValidity() === false) {
            event.preventDefault();
            event.stopPropagation();
          }
          form.classList.add('was-validated');
        }, false);
      });
    }, 
    false );
  })();
</script>
</body>
</html>