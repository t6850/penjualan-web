<?php

class LoginController extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Login');
        $this->load->model('Request/GetUserLoginRequest');
    }

    public function index()
    {
        $this->load->view('Login');
    }

    public function userLogin()
    {
        $getLoginRequest = $this->GetUserLoginRequest;
        $getLoginRequest->setEmail($this->input->post('email'));
        $getLoginRequest->setPassword($this->input->post('password'));
        $userLoginOptional = $this->Login->login($getLoginRequest);
        $httpCode = $userLoginOptional->httpCode;
        if ($httpCode == 200) {
            // $this->session->set_flashdata('success', FlashData::loginSuccess());
            $userData = $userLoginOptional->response;
            $userData = json_decode($userData);
            $userData = $userData->data;
            $userToken = $userData->token;
            $userData = $userData->user;
            $response = array(
                'token' => $userToken,
                'user' => $userData
            );
            $this->session->set_userdata('userAuth', $response);
            redirect('produkcontroller');
        } else {
            // $this->session->set_flashdata('failed', FlashData::loginFailed());
            redirect('logincontroller/index');
        }
    }
}
