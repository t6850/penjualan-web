<?php

class ProdukController extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Produk');
        $this->load->model('Login');
        $this->load->model('Request/GetProdukByKodeRequest');
        $this->load->model('Request/StoreProdukRequest');
    }

    public function index()
    {
        $this->showProduk();
    }

    public function getProdukList()
    {
        // $userAuth = $this->updateUserAuth();
        $getProdukList = $this->Produk->getProdukList();
        $httpCode = $getProdukList->httpCode;
        $getProdukList = json_decode($getProdukList->response);
        $response = array(
            'httpCode' => $httpCode,
            'getProdukList' => $getProdukList,
            'navMenu' => NavMenu::PRODUK
        );
        return $response;
    }

    public function showProduk()
    {
        $response = $this->getProdukList();
        $this->load->view('produk/index.php', $response);
    }

    public function addProduk()
    {
        $response = array(
            'navMenu' => NavMenu::PRODUK
        );
        $this->load->view('produk/add', $response);
    }

    public function storeProduk()
    {
        $StoreProdukRequest = $this->StoreProdukRequest;
        $StoreProdukRequest->setNama($this->input->post('nama'));
        $StoreProdukRequest->setMerk($this->input->post('merk'));
        $StoreProdukRequest->setharga($this->input->post('harga'));
        $produkOptional = $this->Produk->storeProduk($StoreProdukRequest);
        $httpCode = $produkOptional->httpCode;
        if ($httpCode == 200) {
            $this->session->set_flashdata('success', FlashData::successSaved());
        } else {
            $this->session->set_flashdata('failed', FlashData::failedSaved());
        }
        redirect('produkcontroller/showproduk');
    }

    public function updateProduk()
    {
        $StoreProdukRequest = $this->StoreProdukRequest;
        $StoreProdukRequest->setKode($this->input->post('kode'));
        $StoreProdukRequest->setNama($this->input->post('nama'));
        $StoreProdukRequest->setMerk($this->input->post('merk'));
        $StoreProdukRequest->setHarga($this->input->post('harga'));
        $produkOptional = $this->Produk->updateProduk($StoreProdukRequest);
        $httpCode = $produkOptional->httpCode;
        if ($httpCode == 200) {
            $this->session->set_flashdata('success', FlashData::successSaved());
        } else {
            $this->session->set_flashdata('failed', FlashData::failedSaved());
        }
        redirect('produkcontroller/showproduk');
    }

    public function getProdukByKode($kode)
    {
        $GetProdukByKodeRequest = $this->GetProdukByKodeRequest;
        $GetProdukByKodeRequest->setKode($kode);
        $channelOptional = $this->Produk->getByKode($GetProdukByKodeRequest);
        $httpCode = $channelOptional->httpCode;
        $getChannelOptional = json_decode($channelOptional->response);
        $response = array(
            'httpCode' => $httpCode,
            'produkOptional' => $getChannelOptional,
            'navMenu' => NavMenu::PRODUK
        );
        return $response;
    }

    public function optionalProduk($kode)
    {
        $response = $this->getProdukByKode($kode);
        $httpCode = $response['httpCode'];
        if ($httpCode == 200) {
            $this->load->view('produk/edit', $response);
        }else {
            $this->session->set_flashdata('warning', FlashData::dataNotFound());
            redirect('produkcontroller/showproduk');
        }
    }

    public function deleteProduk()
    {
        $GetProdukByKodeRequest = $this->GetProdukByKodeRequest;
        $GetProdukByKodeRequest->setKode($this->input->post('kode'));
        $deleteProduk = $this->Produk->deleteProduk($GetProdukByKodeRequest);
        $httpCode = $deleteProduk->httpCode;
        if ($httpCode == 200) {
            $this->session->set_flashdata('success', FlashData::deletedSuccess());
        } else {
            $this->session->set_flashdata('failed', FlashData::deletedFailed());
        }
        redirect('produkcontroller/showproduk');
    }

    public function updateUserAuth()
    {
        $userData = $this->session->userdata();
        $userData = json_encode($userData);
        $userData = json_decode($userData);
        $userAuth = $userData->userAuth;
        $userToken = $userAuth->token;
        $updateAuth = $this->Login->updateUserAuth($userToken);
        $httpCode = $updateAuth->httpCode;
        if ($httpCode == 200) {
            $userData = $updateAuth->response;
            $userData = json_decode($userData);
            $userData = $userData->data;
            $userToken = $userData->token;
            $userData = $userData->user;
            $response = array(
                'token' => $userToken,
                'user' => $userData
            );
            $this->session->set_userdata('userAuth', $response);
            return $response;
        }
        return $updateAuth;
    }
}
