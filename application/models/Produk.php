<?php

class Produk extends CI_Model
{
    public function getProdukList($token = null)
    {
        $api = BashUrlApi::BASH_URL_API."/api/produkservice/getproduklist";
        $produkList = json_decode(Curl::post($api));
        return $produkList;
    }

    public function storeProduk(StoreProdukRequest $request)
    {
        $api = BashUrlApi::BASH_URL_API."/api/produkservice/storeproduk";
        $storeProduk = json_decode(Curl::post($api, $request->toArray()));
        return $storeProduk;
    }

    public function updateProduk(StoreProdukRequest $request)
    {
        $api = BashUrlApi::BASH_URL_API."/api/produkservice/updateproduk";
        $storeProduk = json_decode(Curl::post($api, $request->toArray()));
        return $storeProduk;
    }

    public function getByKode(GetProdukByKodeRequest $request)
    {
        $api = BashUrlApi::BASH_URL_API."/api/produkservice/getprodukbykode";
        $produk = json_decode(Curl::post($api, $request->toArray()));
        return $produk;
    }

    public function deleteProduk(GetProdukByKodeRequest $request)
    {
        $api = BashUrlApi::BASH_URL_API."/api/produkservice/deleteprodukbycode";
        $produk = json_decode(Curl::post($api, $request->toArray()));
        return $produk;
    }
}
