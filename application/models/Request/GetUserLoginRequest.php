<?php

class GetUserLoginRequest
{
    private $email;
    private $password;

    public function getEmail()
    {
        return $this->email;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function setEmail($email)
    {
        $this->email = $email;
    }

    public function setPassword($password)
    {
        $this->password = $password;
    }

    public function toArray()
    {
        return [
            "email" => $this->getEmail(),
            "password" => $this->getPassword()
        ];
    }
}
