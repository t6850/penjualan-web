<?php

class Login extends CI_Model
{
    public function login(GetUserLoginRequest $request)
    {
        $api = BashUrlApi::BASH_URL_API."/api/loginservice/login";
        $userLogin = json_decode(Curl::post($api, $request->toArray()));
        return $userLogin;
    }

    public function updateUserAuth($token)
    {
        $api = BashUrlApi::BASH_URL_API."/api/loginservice/updateuserauth";
        $userLogin = json_decode(Curl::post($token, $api));
        return $userLogin;
    }
}
